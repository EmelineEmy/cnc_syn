<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Boutique</title>
    <link rel="stylesheet" href="./style.css">
    <link href="https://fonts.cdnfonts.com/css/kiona-2" rel="stylesheet">
</head>
<body>
    <?php include './header.php';
        include "./pdo.php";
        $id =$_POST['id']?>
        
            <?php
                include "./admin/DB/boutiquef.php";
                $carte = readAllProducts();
                foreach($carte as $data){
                    if($data['dispo']){
                    ?>
                    <div class="container boutique produits">
                        <img src="<?php echo $data['img']?>" alt="" srcset="">
                        <div class="container text">
                            <div class="container produits nom">
                                <h2><?php echo $data['nom'] ?></h2>
                            </div>
                            <div class="container produits description">
                                <p><?= $data['descri']?></p>
                            </div>
                            <div class="container produits audio">
                            <audio controls width="300" height="32" autoplay>
                            <source src="<?= $data['audio'] ?>" type="audio/mpeg"> 
                        </div>
                        </div>
                            <div class="container other prix">
                                    <div class="container produits prix">
                                        <p><span><?= $data['prix'] ?></span>€</p>
                                    </div>
                            </div>
                     
                            <div class="button add">
                                <form action="ajouter.php" method="post">
                                    <input type="hidden" name="id_prod" value="<?= $data['id'] ?>">
                                    
                                    <input type="submit" value="Ajouter au panier" id="add">
                                </form>
                            </div>
                            
                            <?php $id = $data['id'];?>
                    </div>
    <?php }} ?>

</body>




</html>