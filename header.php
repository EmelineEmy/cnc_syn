<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Header</title>
    <link rel="stylesheet" href="./style.css">
    <link href="https://fonts.cdnfonts.com/css/salute-riches-free" rel="stylesheet">
</head>
<body class="test header">
    <div class="fixation">
        <div class="container header">
            <a href="./homepage.php"><img src="./others/images/MUSCYN.svg" alt=""></a>
            <p>MUSCYN</p>
            <div class="other page">
                <a href="./boutique.php"><img src="./others/images/shop.svg" alt=""></a>
                <a href="./panier.php"><img src="./others/images/basket.svg" alt=""></a>
            </div>
        </div>
    </div>
</body>
</html>