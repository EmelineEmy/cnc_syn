<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connexion Administrateur</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.cdnfonts.com/css/kiona-2" rel="stylesheet">

</head>
<body class="connexion administateur">

<div class="container form connexion administrateur">
    <form action="./admin/DB/validate.php" method="post">
                <div class="title connexion administrateur">
                    <p> Connexion </p>
                </div>
            <div class="content form connexion administrateur">
                <div class="textbox">
                    <i class="fa fa-user" aria-hidden="true"></i>
                    <input type="text" placeholder="Username"
                            name="username" value="">
                    <div class="ligne"></div>
                </div>
    
                <div class="textbox">
                    <i class="fa fa-lock" aria-hidden="true"></i>
                    <input type="password" placeholder="Password"
                            name="pass" value="">
                            <div class="ligne"></div>

                </div>
            
            <div class="button form connexion administrateur">
                <input class="button" type="submit"
                        name="login" value="Connexion">
            </div>
        </div>
    </form>
</div>


</body>
</html>