<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);  // montre les erreurs dans les pages
error_reporting(E_ALL);?>


<?php
if( isset($_POST['id_prod']) 
&&  isset($_POST['qt']) 
){
    $id_cmd = $_COOKIE['id_cmd'];
    $id_prod = $_POST['id_prod'];
    $qt = $_POST['qt'];

    include('./pdo.php');

    if($id_cmd == ""){
        $req = $pdo->prepare('insert into commande() values ();');
        $req->execute();
        $id_cmd = $pdo->lastInsertId();
        setcookie("id_cmd", $id_cmd);
    }

    $req = $pdo->prepare('INSERT INTO produit_commande (id_commande,id_produit,qt) VALUES (?,?,?) ON DUPLICATE KEY UPDATE qt=qt+?;');
    $req->execute([$id_cmd, $id_prod, $qt, $qt]);
}

header('location: panier.php');

?>