<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Panier</title>
    <link rel="stylesheet" href="style.css">
</head>
<body class="panier body test">

<?php include 'header.php'; ?>
<?php
        require('pdo.php');
        
        $req = $pdo->prepare('select * from ligne_commande where id_commande = ?;');
        $req->execute([$_COOKIE['id_cmd']]);
        $mesInfos = $req->fetchAll();
        
        $total = 0;
        
        foreach($mesInfos as $data){
            $prod = $pdo->query("select * from product where id = ${data['id_produit']};")->fetch();
            $total += $prod['prix'];
            
            ?>
<div class="container panier all">
<div class="containerde container panier recuperation">
    <div class="container récupération panier">
        <div class="content panier recuperation">
            <div class="text content panier recuperation">
                <?= $prod['nom'] ?> </p>
            </div>
            <img src="<?php echo $prod['img'] ?>" alt="">
            <div class="price content panier recuperation">
                <p><?= $prod['prix'] ?>€  </p>
            </div>
            
            <form action="">
                <input type="hidden" name="c" value="c">
                <input type="hidden" name="id_prod" value="<?= $data['id_produit'] ?>">
                <input type="submit" value="X">
            </form>
        </div>
    </div>
    TOTAL : <?= $total ?>€
</div>
<?php } ?>

<div class="container form panier">
        <form action="validation-panier.php" method="post">
                <input type="text" name="nom" placeholder="nom">
                <input type="text" name="mail" placeholder="mail">
                <input type="text" name="tel" placeholder="telephone">
                <div class="button submit panier">
                    <input type="submit" value="VALIDER MA COMMANDE">
                </div>
        </form>
    </div>
</div>

<?php 
    
    if(isset($_GET['c'])){
        $id_prod= $_GET['id_prod'];
        $req = $pdo->prepare('UPDATE  product SET dispo =? where id=?;');
        $req->execute([1, $id_prod]);
        $req = $pdo->prepare('DELETE FROM ligne_commande where  id_commande=? and id_produit=?;');
        $req->execute([$_COOKIE['id_cmd'], $id_prod]);
        
    }
    ?>

</body>
</html>