drop database if exists cncsyn;
create database cncsyn;
use cncsyn; 

create table info_boutique(
    adresse varchar(255) not null,
    tel varchar(255) not null,
    horaire varchar(255) not null
);
insert into info_boutique(adresse, tel, horaire)
values (
    '302 Rue Lamarck, 11000 Carcassonne',
    012345678,
    '9h00 à 16h00'
);

create table client(
    id int not null auto_increment primary key,
    nom varchar(255) not null,
    mail varchar(255) not null,
    tel varchar(255) not null
);

create table commande(
    id int auto_increment primary key,
    id_client int,
    etat enum('panier', 'validée', 'prete', 'collectée') default 'panier'
);

create table ligne_commande(
    id_commande int,
    id_produit int
);

create table product(
    id int not null auto_increment primary key, 
    nom varchar(255), 
    img text, 
    prix float, 
    dispo boolean default 1, 
    descri varchar(255),
    audio text
);

insert into product(nom, img, prix,dispo, descri, audio)
values (
    'Guitare N°4324',
    'https://images.unsplash.com/photo-1541689592655-f5f52825a3b8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTZ8fGd1aXRhcmV8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60',
    13,
    1,
    'Guitare pour les débutants, corde non collante aux doigts',
    ''
);

create table adminlogin(
    id int(11) not null auto_increment,
    username varchar(100) not null,
    pass varchar(100) not null,
    primary key (id)
)    engine=MyISAM default charset=latin1;


insert into adminlogin (id, username, pass)
values (
   NULL,
   'admin',
   'muscynadmin' 
);

drop user if exists toto@'127.0.0.1';
create user toto@'127.0.0.1' identified by 'salut';
grant all privileges on cncsyn.* to toto@'127.0.0.1';