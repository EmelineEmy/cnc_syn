<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Création des produits</title>
    <link rel="stylesheet" href="../style.css">
</head>
<body class="produit creation">
<?php include 'header.php';?>
<div class="container form produit creation">
    <div class="creation border top container"> AJOUT PRODUIT </div>
    <form action="../admin/DB/createProducts.php" method="post"  class="nul">   
            <div class="container products admin">
                <div class="image products">
                    <label for="img"></label>
                    <input type="text" id="id" name="img" >
                </div>
                <div class="text products">
                    <label for="nom"></label>
                    <input type="text" id="Password" name="nom" placeholder="Nom du Produit">
                </div>
                <div class="price products">
                    <label for="Password"></label>
                    <input type="number" id="Password" name="prix" step="0" placeholder="Prix">
                </div>
                <div class="descri products">
                    <label for=""></label>
                    <input type="text" name="descri" placeholder="description">
                </div>
                <div class="audio">
                    <label for=""></label>
                    <input type="file" name="audio" value="audio">
                </div>
                <div class="button products">
                    <label for="button"></label>
                    <input type="submit" id="button" value="Création">
                </div>
            </div>
        </form>
    </div>
</body>
</html>
