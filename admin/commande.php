<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>COMMANDES</title>
    <link rel="stylesheet" href="../style.css">
    <link href="https://fonts.cdnfonts.com/css/kiona-2" rel="stylesheet">

</head>
<body class="commande retour client">
<?php include'./header.php';?>
<div class="test commandes des cartes">
<?php
        require('../pdo.php');
        $req = $pdo->query('select * from commande;');
        $mesInfos = $req->fetchAll();
            foreach($mesInfos as $cmd){
            if($cmd['id_client'] != NULL){
                $req = $pdo->query("select * from client where id=${cmd['id_client']};");
                $client = $req->fetch();
            }else{
                $client = [
                    "id" => '',
                    "nom" => '',
                    "tel" => '',
                    "mail" => ''
                ];
            }
    ?>
    <div class="commandes id client">
        <div class="commandes nom mail tel client">
            <span><?= $cmd['id'] ?></span>
            <span><?= $client['nom'] ?></span>
            <span><?= $client['mail'] ?></span>
            <span><?= $client['tel'] ?></span>
        </div>
        <div class="cardsssss">
            <?php
            $req = $pdo->prepare('select * from ligne_commande where id_commande = ?;');
            $req->execute([$cmd['id']]);
            $mesInfos = $req->fetchAll();
            $total = 0;
            foreach($mesInfos as $data){
                $prod = $pdo->query("select * from product where id = ${data['id_produit']};")->fetch();
                $total += $prod['prix'];
        ?>
            <div class="container retour produit commandes">
                    <div class="nom retour commandes">
                        <p><?=  $prod['nom'] ?> </p>
                    </div>
                    <img src="<?php print_r($prod['img'])?>" alt="" srcset=""> 
                    <div class="prix retour commandes">
                        <p><?= $prod['prix'] ?>€ </p>
                    </div>
                </div>
            <?php } ?>
            <div class="container total commande">
                <div class="total commande resultat">
                    TOTAL : <?= $total ?>€
                </div>
            ETAT CMD : <?= $cmd['etat'] ?>
            <?php
                if($cmd['etat'] != 'panier'){
                    ?>
                    <div class="changer etat button">
                        <a href="changerEtat.php?id=<?= $cmd['id'] ?>">OK</a>
                    </div>
            </div>
<?php
                }
            ?>
        </div>
    </div>
    <?php } ?>
</div> 
</body>
</html>