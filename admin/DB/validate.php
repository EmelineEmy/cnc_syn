


<?php
 
include 'connexion.php';
  
function test_input($data) {
     
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
  
if ($_SERVER["REQUEST_METHOD"] == "POST") {
     
    $username = test_input($_POST["username"]);
    $password = test_input($_POST["pass"]);
    $stmt = $conn->prepare("SELECT * FROM adminlogin;");
    $stmt->execute();
    $users = $stmt->fetchAll();
     
    foreach($users as $user) {
         
        if(($user['username'] == $username) &&
            ($user['pass'] == $password)) {
                session_start();
                $_SESSION["admin"] = true;
                header("location: ../HomeA.php");
        }
        else {
            echo "<script language='javascript'>";
            echo "alert('WRONG INFORMATION')";
            echo "</script>";

            
            // header('location: ../../connexion.php');
        }
        
    }
}
 
?>