
<?php
include './pdo.php';



function createProduct($nom,$img,$prix, $descri, $audio){
    global $pdo;
    $req = $pdo->prepare('insert into product (nom, img, prix,descri, audio) value (?,?,?,?,?);');
    $req->execute([$nom, $img, $prix, $descri, $audio]);
    
}

function readAllProducts(){
    global $pdo; 
    $req = $pdo->query("select * from product;");
    return $req->fetchAll(); 
}

function updateProducts($id, $nom, $img, $prix, $dispo, $descri, $audio  ){
    
    global $pdo;
    $req = $pdo->prepare("update product set img=? ,nom=?, prix=?, dispo=?, descri=?, audio=? where id=?;");
    $req->execute([$img, $nom, $prix, $dispo,$descri, $audio, $id]);
   }


   function readProducts($id){
    global $pdo; 
    $req = $pdo->query("select * from product where id = ${id};");
    return $req->fetchAll(); 
}
   
function deleteProducts($id){
    global $pdo;
    $req = $pdo->prepare("delete from product where id=?;");
    $req->execute([$id]);
}

?>