
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);  // montre les erreurs dans les pages
error_reporting(E_ALL);

$user = 'toto'; // user name
$pass = 'salut'; // password
$host = '127.0.0.1'; // ip address
$db   = 'cncsyn'; // nom de la base de données

$pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass); // accès à la connexion
?>