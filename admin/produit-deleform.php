<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Suppression des produits</title>
    <link rel="stylesheet" href="../style.css">
</head>
<body class="produit delete form">

     <?php include 'header.php';?>
    <?php 
        include './DB/boutiquef.php';
        include './DB/pdo.php';
        $id =$_GET['id'];
        $carte = readProducts($id)[0];
    ?>  
<div class="container container product form delete">
    <div class="container delete form">
        <form action="./DB/deleteProducts.php">
            <p>Voulez vous vraiment supprimer le produit ?</p>
            <input type="hidden" value="<?= $id ?>" id="inputmenu" name='lid'>
            <input type="submit" value="Oui">
        </form>
    </div>
    <div class="container modification form">
    <div class="modification border top container">PRODUIT A SUPPRIMER</div>
        <div class="container modification produit">
                        <img src="<?php echo $carte['img']?>" alt="" srcset="">
                        <div class="container modification">
                            <div class="container modification nom">
                                <h2><?php echo $carte['nom'] ?></h2>
                            </div>
                            <div class="container modification description">
                                <p><?= $carte['descri']?></p>
                            </div>
                            <div class="container modification audio">
                            <audio controls width="300" height="32" autoplay>
                            <source src="<?= $carte['audio'] ?>" type="audio/mpeg"> 
                        </div>
                        </div>
                            <div class="container other prix">
                                    <div class="container produits prix">
                                        <p><span><?= $carte['prix'] ?></span>€</p>
                                    </div>
                            </div>
                        </div>
</div>
</body>
</html>