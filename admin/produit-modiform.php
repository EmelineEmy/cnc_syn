<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modification des produits</title>
</head>
<body class="produit modification form">
<?php include 'securite.php'?>
<div></div>
<?php include './header.php' ?>
<?php 
include '../DB/updateProducts.php';
include './DB/boutiquef.php';
include './DB/pdo.php';
$id =$_GET['id'];
$carte = readProducts($id)[0];
?>
<div class="container create form product">
    <div class="form create produit">
        <form action="./DB/updateProducts.php" method="post" >
            <div class="modification border top container"> MODIFICATION PRODUIT     </div>
            <div class="image products">
                <input type="text" name="img">
            </div>    
            <div class="text products modification">
                <input type="text" name="nom" placeholder="Nom">
            
                <input type="text" name="descri" placeholder="Description" >

                <input type="number" name="prix" placeholder="prix">

                <input type="file" name="audio" placeholder="audio">

                <input type="checkbox" name="dispo" min="0" max="1">
            </div>
                <div class="button products modification">
                    <input type="hidden" value="<?= $id ;?>" id="" name="id">
                    <input type="submit" value="Modifier">
                </div>
        </form>
    </div>
    <div class="container modification form">
    <div class="modification border top container">PRODUIT SELECTIONNER</div>
        <div class="container modification produit">
                        <img src="<?php echo $carte['img']?>" alt="" srcset="">
                        <div class="container modification">
                            <div class="container modification nom">
                                <h2><?php echo $carte['nom'] ?></h2>
                            </div>
                            <div class="container modification description">
                                <p><?= $carte['descri']?></p>
                            </div>
                            <div class="container modification audio">
                            <audio controls width="300" height="32" autoplay>
                            <source src="<?= $carte['audio'] ?>" type="audio/mpeg"> 
                        </div>
                        </div>
                            <div class="container other prix">
                                    <div class="container produits prix">
                                        <p><span><?= $carte['prix'] ?></span>€</p>
                                    </div>
                            </div>
                        </div>
</div>
</body>
</html>