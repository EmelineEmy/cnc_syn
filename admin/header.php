<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Header</title>
    <link rel="stylesheet" href="../style.css">
    <link href="https://fonts.cdnfonts.com/css/kiona-2" rel="stylesheet">
</head>
<body>
<?php include'securite.php'?>

    <div class="container homepageA navbar">
        <a href=""><img src="../others/images/MUSCYN.svg" alt="" srcset=""></a>
        <div class="container titre selection modification">
            <div class="title">
                Page d'accueil
            </div>
            <div class="lien page ">
                <a href="./HomeA.php"> HomePage Administateur</a>
                <a href="./Home-modif.php">Modification des informations</a>
            </div>
        </div>
        <div class="container titre selection modification">
            <div class="title">
                Page Produits
            </div>
            <div class="lien page">
                <a href="./produit-creation.php">Ajout des produits</a>
                <a href="./produit-modifications.php"> Modification des produits</a>
                <a href="./produit-delete.php">Suppression des produits</a>
            </div>
        </div>
        <div class="container titre selection modification">
            <div class="title">
                Page des commandes
            </div>
            <div class="lien page">
                <a href="./commande.php">Suivi des commandes</a>
            </div>
            <?php session_start();
             if($_SESSION['admin']){ ?>
            <form class="deco">
                <div class="button deconnection">
                <input type="hidden" name="cacher" value="cacher">
                    <input type="submit" value="" src="../others/images/exit.svg">
                </div>
            </form>
            <?php  } ?>
        </div>
    </div>
   

    <?php 
    if(isset($_GET['cacher'])){
        $_SESSION['admin'] = false;
        header("location: ../connexion.php");
    }
    ?>
</body>
</html>