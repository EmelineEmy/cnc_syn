<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modification HomePage</title>
    <link rel="stylesheet" href="../style.css">
    <link href="https://fonts.cdnfonts.com/css/kiona-2" rel="stylesheet">

</head>
<body class="modification homepage">
<?php include 'header.php';?>

<?php 
include "./admin/DB/pdo.php";
include "./DB/homepagef.php";
?>
    <div class="container homepageinformation">
        <div class="creation border top container"> MODIFICATION DES INFORMATIONS </div>

            <form action="./DB/updateHomepage.php" method="post">
                <div class="text modification" #thisinput>
                    <input type="text" name="adresse" placeholder="adresse">
                    <div class="ligne"></div>
                    <input type="text" name="horaire" placeholder="horaire">
                    <div class="ligne"></div>
                    <input type="tel" name="tel" placeholder="téléphone" pattern="[0-9]{2}-[0-9]{2}-[0-9]{2}-[0-9]{2}-[0-9]{2}"required>
                    <div class="ligne"></div>
                </div>
                <div class="button hompeageinformation">
                    <input type="submit" value="Modifier">
                </div>
            </form>
    </div>



</body>
</html>